//
//  ViewController.m
//  car
//
//  Created by Юрий Трыков on 23.08.17.
//  Copyright © 2017 trykov. All rights reserved.
//

#import "ViewController.h"
#import "UIView+Animation.h"

@interface ViewController ()

@property(strong, nonatomic) IBOutlet UIView *carView;

@end

@implementation ViewController

#pragma mark UIResponder

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:touch.view];
    [self moveCar:location];
}

#pragma mark Private Methods

- (void)moveCar:(CGPoint)location {
    float angle = [self getAngle:[self topCenter] :location];
    [self.carView rotate:angle completion:^(BOOL i) {
        [self.carView move:location completion:nil];
    }];
}

- (float)getAngle:(CGPoint)a :(CGPoint)b {
    CGFloat angle = atan2f(b.y - a.y, b.x - a.x);
    return angle;
}


- (CGPoint)topCenter {
    CGRect frame = self.carView.frame;
    CGPoint topCenter = CGPointMake(CGRectGetMidX(frame), CGRectGetMaxY(frame));
    return topCenter;
}

@end
