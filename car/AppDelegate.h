//
//  AppDelegate.h
//  car
//
//  Created by Юрий Трыков on 23.08.17.
//  Copyright © 2017 trykov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

