//
//  UIView+Animation.h
//  car
//
//  Created by Юрий Трыков on 23.08.17.
//  Copyright © 2017 trykov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^animateCompletion) (BOOL a);


@interface UIView (Animation)

/*
 * Метод поворота view 
 * @param degree - угол поворота, в радианах
 * @completion - блок окончания анимации
 */
- (void)rotate:(float)angle completion:(animateCompletion)completion;

/*
 * Метод перемещения центра view
 * @param endPoint - конечная точка
 * @completion - блок окончания анимации
 */
- (void)move:(CGPoint)endPoint completion:(animateCompletion)completion;

@end
