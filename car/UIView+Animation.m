//
//  UIView+Animation.m
//  car
//
//  Created by Юрий Трыков on 23.08.17.
//  Copyright © 2017 trykov. All rights reserved.
//

#import "UIView+Animation.h"

@implementation UIView (Animation)

- (void)rotate:(float)angle completion:(animateCompletion)completion {
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.transform = CGAffineTransformRotate(CGAffineTransformIdentity, angle);
                     }
                     completion:completion];
}

- (void)move:(CGPoint)endPoint completion:(animateCompletion)completion{
    [UIView animateKeyframesWithDuration:1.5
                                   delay:0.1
                                 options:UIViewKeyframeAnimationOptionCalculationModeLinear
                              animations:^{
                                  self.center = endPoint;
                              }
                              completion:completion];
}
@end
